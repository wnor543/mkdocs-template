## TaskList

```markdown
* [x] Lorem ipsum dolor sit amet, consectetur adipiscing elit
* [x] Nulla lobortis egestas semper
* [x] Curabitur elit nibh, euismod et ullamcorper at, iaculis feugiat est
* [ ] Vestibulum convallis sit amet nisi a tincidunt
    * [x] In hac habitasse platea dictumst
    * [x] In scelerisque nibh non dolor mollis congue sed et metus
    * [x] Sed egestas felis quis elit dapibus, ac aliquet turpis mattis
    * [ ] Praesent sed risus massa
* [ ] Aenean pretium efficitur erat, donec pharetra, ligula non scelerisque
* [ ] Nulla vel eros venenatis, imperdiet enim id, faucibus nisi
```

* [x] Lorem ipsum dolor sit amet, consectetur adipiscing elit
* [x] Nulla lobortis egestas semper
* [x] Curabitur elit nibh, euismod et ullamcorper at, iaculis feugiat est
* [ ] Vestibulum convallis sit amet nisi a tincidunt
    * [x] In hac habitasse platea dictumst
    * [x] In scelerisque nibh non dolor mollis congue sed et metus
    * [x] Sed egestas felis quis elit dapibus, ac aliquet turpis mattis
    * [ ] Praesent sed risus massa
* [ ] Aenean pretium efficitur erat, donec pharetra, ligula non scelerisque
* [ ] Nulla vel eros venenatis, imperdiet enim id, faucibus nisi


-----

## HighLight/Mark

```markdown
==highlight==
```

==highlight==

-----

## Inline Hilite

```markdown
`#!js var test = 0;`
```

`#!js var test = 0;`

----

## Latex(default: MathJax)

```markdown
$$
\frac{n!}{k!(n-k)!} = \binom{n}{k}
$$
```

$$
\frac{n!}{k!(n-k)!} = \binom{n}{k}
$$

$$
E(\mathbf{v}, \mathbf{h}) = -\sum_{i,j}w_{ij}v_i h_j - \sum_i b_i v_i - \sum_j c_j h_j
$$

\[3 < 4\]

\begin{align}
    p(v_i=1|\mathbf{h}) & = \sigma\left(\sum_j w_{ij}h_j + b_i\right) \\
    p(h_j=1|\mathbf{v}) & = \sigma\left(\sum_i w_{ij}v_i + c_j\right)
\end{align}

```math
\begin{align}
    p(v_i=1|\mathbf{h}) & = \sigma\left(\sum_j w_{ij}h_j + b_i\right) \\
    p(h_j=1|\mathbf{v}) & = \sigma\left(\sum_i w_{ij}v_i + c_j\right)
\end{align}
```

-----

## B64

Embedding local *png*, *jpeg* and *gif* with base64 encoding.

```markdown
![picture](../assets/bg.png)
```

![picture](./assets/qrcode.png)

----- 

## Better Em

Aims to improve emphasis (bold and italic) handling.

```markdown
* Won't highlight *

*Will highlight*
```

* Won't highlight *

*Will highlight*

```markdown
***I'm italic and bold* I am just bold.**

***I'm bold and italic!** I am just italic.*
```

***I'm italic and bold* I am just bold.**

***I'm bold and italic!** I am just italic.*

```markdown
___A lot of underscores____________is okay___

___A lot of underscores____________is okay___
```

___A lot of underscores____________is okay___

___A lot of underscores____________is okay___


```markdown
`*All will * be italic*

`*All will *be italic*

`*All will not* be italic*

`*All will not ** be italic*

`**All will * be bold**

`**All will *be bold**

`**All will not*** be bold**

`**All will not *** be bold**
```

`*All will * be italic*

`*All will *be italic*

`*All will not* be italic*

`*All will not ** be italic*

`**All will * be bold**

`**All will *be bold**

`**All will not*** be bold**

`**All will not *** be bold**

----

## Caret

Caret optionally adds two different features which are syntactically built around the `^` character. 

### Insert

To wrap content in an insert tag, simply surround the text with double `^`.

```markdown
^^Insert This^^
```

^^Insert This^^

### Superscript

To denote a superscript, you can surround the desired content in single ^. It uses Pandoc style logic, so if your superscript needs to have spaces, you must escape the spaces.

```markdown
H^2^0

text^a\ superscript^
```

H^2^0

text^a\ superscript^

-----

## Critic

Critic is an extension that adds handling and support of [Critic Markup](http://criticmarkup.com/) which uses a special syntax to represent edits to a Markdown document.

Critic Markup uses special markup to insert, delete, substitute, highlight, and comment.

```markdown
{++insert me++}
{--remove me--}
{~~substitute this~>with this~~}
{==highlight me==}
{==highlight me==}{>>Add a comment<<}
```

{++insert me++}
{--remove me--}
{~~substitute this~>with this~~}
{==highlight me==}
{==highlight me==}
{>>Add a comment<<}


```markdown
Here is some {--*incorrect*--} Markdown.  I am adding this{++ here++}.  Here is some more {--text
 that I am removing--}text.  And here is even more {++text that I 
 am ++}adding.{~~

~>  ~~}Paragraph was deleted and replaced with some spaces.{~~  ~>

~~}Spaces were removed and a paragraph was added.

And here is a comment on {==some
 text==}{>>This works quite well. I just wanted to comment on it.<<}. Substitutions {~~is~>are~~} great!

General block handling.

{--

* test remove
* test remove
* test remove
    * test remove
* test remove

--}

{++

* test add
* test add
* test add
    * test add
* test add

++}
```

Here is some {--*incorrect*--} Markdown.  I am adding this{++ here++}.  Here is some more {--text
 that I am removing--}text.  And here is even more {++text that I 
 am ++}adding.{~~

~>  ~~}Paragraph was deleted and replaced with some spaces.{~~  ~>

~~}Spaces were removed and a paragraph was added.

And here is a comment on {==some
 text==}{>>This works quite well. I just wanted to comment on it.<<}. Substitutions {~~is~>are~~} great!

General block handling.

{--

* test remove
* test remove
* test remove
    * test remove
* test remove

--}

{++

* test add
* test add
* test add
    * test add
* test add

++}

-----

## Details

Details is an extension that creates collapsible elements that hide their content. It uses the HTML5 `#!html <details><summary>` tags to accomplish this. It supports nesting and you can also force the default state to be open. And if you want to style some different than others, you can optionally feed in a custom class.

Details must contain a blank line before they start. Use `???` or `???+`

Supported Type: `note`, `success`, `warning classes`, `tip`, `info`

```markdown
???+ note "Open styled details"

    ??? danger "Nested details!"
        And more content again.
```

???+ note "Open styled details"

    ??? danger "Nested details!"
        And more content again.


-----

## Emoji

The Emoji extension adds support for inserting emoji via simple short names enclosed within colons: `:short_name:`. Supported Emoji Providers: [Gemoji](https://github.com/github/gemoji), [EmojiOne](https://github.com/Ranks/emojione), [Twemoji](https://github.com/twitter/twemoji)

```markdown
:smile: :heart: :thumbsup:
```

:smile: :heart: :thumbsup:

-----

## HighLight

```python
from sanic import Sanic
from sanic.response import json

app = Sanic()

@app.route('/', methods=['GET'])
async def index(req):
  return json({'hello': 'world'})

if __name__ == '__mian__':
  app.run(host='0.0.0.0', port=8080, debug=True)
```


----

## InlineHilite

InlineHilite is an inline code highlighter, the syntax is: 
```markdown
`:::language mycode`

or 

`#!language mycode`
```

Here is some code: `#!js function pad(v){return ('0'+v).split('').reverse().splice(0,2).reverse().join('')}`.

The mock shebang will be treated like text here: ` #!js var test = 0; `.

```markdown
`#!math p(x|y) = \frac{p(y|x)p(x)}{p(y)}`
```

`#!math p(x|y) = \frac{p(y|x)p(x)}{p(y)}`

----

## [Keys](https://facelessuser.github.io/pymdown-extensions/extensions/keys/)

Keys is an extension to make entering and styling keyboard key presses easier. Syntactically, Keys is built around the `+` symbol. A key or combination of key presses is surrounded by `++` with each key press separated with a single `+`.

```markdown
++ctrl+alt+"My Special Key"++

++cmd+alt+"&Uuml;"++
```

++ctrl+alt+"My Special Key"++

++cmd+alt+"&Uuml;"++

----

## [MagicLink](https://facelessuser.github.io/pymdown-extensions/extensions/magiclink/)

MagicLink is an extension that provides a number of useful link related features. MagicLink can auto-link HTML, FTP, and email links. It can auto-convert repository links (GitHub, GitLab, and Bitbucket) and display them in a more concise, shorthand format. MagicLink can also be configured to directly auto-link the aforementioned shorthand format.

### Auto-Linking

  - Just paste links directly in the document like this: https://google.com.
  - Or even an email address: fake.email@email.com.

### Mentions(`@{provider}:{user}/{repo}`, the default provider is github)
  - @facelessuser
  - twitter:twitter
  - @gitlab:pycqa/flake8

### Issues and Pull Requests(Issues and pull requests are specified with `#{num}` and `!{num}` respectively. And to reference an external provider, use the format `{provider}:{user}/{repo}#{num}`)
  - Pull request Python-Markdown/markdown!598
  - Pull request gitlab:pycqa/flake8!213
  - Issue gitlab:pycqa/flake8#385

------

## ProgressBar

ProgressBar is an extension that adds support for progress/status bars. It can take percentages or fractions, and it can optionally generate classes for percentages at specific value levels.

`[= <percentage or fraction> "optional single or double quoted title"]`

```markdown
[=85% "85%"]
[=100% "100%"]
[=25%]{: .thin}
[=45%]{: .thin}
```

[=85% "85%"]
[=100% "100%"]

[=25%]{: .thin}
[=45%]{: .thin}

------

## SmartSymbols

SmartSymbols adds syntax for creating special characters such as trademarks, arrows, fractions, etc. It basically allows for more "smarty-pants" like replacements. It is meant to be used along side Python Markdown's smarty extension not to replace.

```markdown
(tm)
(c)
(r)
c/o
+/-
-->
<--
<-->
=/=
1/4, etc.
1st 2nd etc.
```

(tm)
(c)
(r)
c/o
+/-
-->
<--
<-->
=/=
1/4, etc.
1st 2nd etc.

----

## [Snippets](https://facelessuser.github.io/pymdown-extensions/extensions/snippets/)

Snippets is an extension to insert markdown or HTML snippets into another markdown file. Snippets is great for situations where you have content you need to insert into multiple documents. For instance, this document keeps all its hyperlinks in a separate file. Then includes those hyperlinks at the bottom of a document via snippets. If a link needs to be updated, it can be updated in one location instead of updating them in multiple files.

```markdown
--8<-- "escaped notation"<space>

--8<--<space>
escaped notation
--8<--<space>
```


--8<-- "index.md"  

--8<-- 
index.md
; howto.md
--8<-- 

-------

## SuperFences

SuperFences provides a number of features:
+ Allowing the nesting of fences under blockquotes, lists, or other block elements (see Limitations for more info).
+ Create tabbed fenced code blocks.
+ Ability to specify custom fences to provide features like flowcharts, sequence diagrams, or other custom blocks.
+ Allow disabling of indented code blocks in favor of only using the fenced variant (off by default).
+ Experimental feature that preserves tabs within a code block instead of converting them to spaces which is Python Markdown's default behavior.


### Tabbed Fences

SuperFences has the ability to create tabbed code blocks. Simply add `tab="tab-title"` to the fence's header, and the code block will will be rendered in a tab with the title `tab-title`. If you do not provide a title with either `tab=` or `tab=""`, but you've specified a language, the language will be used as the title. No case transformation is applied – what you type is what you get. Consecutive code tabs will be grouped together.



    ```Bash tab=
    #!/bin/bash
    STR="Hello World!"
    echo $STR
    ```

    ```C tab=
    #include 

    int main(void) {
      printf("hello, world\n");
    }
    ```

    ```C++ tab=
    #include <iostream>

    int main() {
      std::cout << "Hello, world!\n";
      return 0;
    }
    ```

    ```C# tab= hl_lines="1 3"
    using System;

    class Program {
      static void Main(string[] args) {
        Console.WriteLine("Hello, world!");
      }
    }
    ```


```Bash tab=
#!/bin/bash
STR="Hello World!"
echo $STR
```

```C tab=
#include 

int main(void) {
  printf("hello, world\n");
}
```

```C++ tab= linenums="1 1 2"
## linenums="1 1 2" : params: start_num start, step
#include <iostream>

int main() {
  std::cout << "Hello, world!\n";
  return 0;
}
```

```C# tab= hl_lines="1 3"
using System;

class Program {
  static void Main(string[] args) {
    Console.WriteLine("Hello, world!");
  }
}
```

+ flow charts

```markdown tab="flow source"
    ```flow
    st=>start: Start:>http://www.google.com[blank]
    e=>end:>http://www.google.com
    op1=>operation: My Operation
    sub1=>subroutine: My Subroutine
    cond=>condition: Yes
    or No?:>http://www.google.com
    io=>inputoutput: catch something...

    st->op1->cond
    cond(yes)->io->e
    cond(no)->sub1(right)->op1
    ```
```

```flow tab="flow result"
st=>start: Start:>http://www.google.com[blank]
e=>end:>http://www.google.com
op1=>operation: My Operation
sub1=>subroutine: My Subroutine
cond=>condition: Yes
or No?:>http://www.google.com
io=>inputoutput: catch something...

st->op1->cond
cond(yes)->io->e
cond(no)->sub1(right)->op1
```

+ Sequence Diagram

```markdown tab="sequence source"
    ```sequence
    Title: Here is a title
    A->B: Normal line
    B-->C: Dashed line
    C->>D: Open arrow
    D-->>A: Dashed open arrow
    ```
```

```sequence tab="sequence result"
Title: Here is a title
A->B: Normal line
B-->C: Dashed line
C->>D: Open arrow
D-->>A: Dashed open arrow
```

